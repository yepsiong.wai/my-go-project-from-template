# Introduction

This is a template for doing go-micro development using GitLab. It's based on the
[helloworld](https://github.com/micro/examples/tree/master/helloworld) Go Micro
template.

# Reference links

- [GitLab CI Documentation](https://docs.gitlab.com/ee/ci/)
- [Go Micro Overview](https://micro.mu/docs/go-micro.html)
- [Go Micro Toolkit](https://micro.mu/docs/go-micro.html)

# Getting started

- To generate swagger-ui docs, run `${GOPATH}/bin/swag init`

## What's contained in this project

- main.go - is the main definition of the service, handler and client
- router.go - echo route definitions
- validator.go - echo validators

## Dependencies

Install the following

- [micro](https://github.com/micro/micro)
- [protoc-gen-micro](https://github.com/micro/protoc-gen-micro)

## Run Service

```shell
go run .
```

## Query Service

```
micro call greeter Greeter.Hello '{"name": "John"}'
```
