package main

import (
	"github.com/labstack/echo/v4"
	echoSwagger "github.com/swaggo/echo-swagger"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"net/http"

	_ "ysw/my-go/docs"
)

var db *gorm.DB

type EventEntity struct {
	gorm.Model
	Code          string
	ConsignmentNo string `gorm:"unique"`
	Remarks       string
}

type EventDto struct {
	Id            uint   `json:"id"`
	Code          string `json:"eventCode"`
	ConsignmentNo string `json:"consignmentNo"`
	Remarks       string `json:"remarks"`
}

// @title           Swagger Example API
// @version         1.0
// @description     This is a sample server celler server.
// @termsOfService  http://swagger.io/terms/

// @contact.name   API Support
// @contact.url    http://www.swagger.io/support
// @contact.email  support@swagger.io

// @license.name  Apache 2.0
// @license.url   http://www.apache.org/licenses/LICENSE-2.0.html

// @host      localhost:8888
// @BasePath  /api/v1

// @securityDefinitions.basic  BasicAuth

// @securityDefinitions.apikey  ApiKeyAuth
// @in                          header
// @name                        Authorization
// @description					Description for what is this security definition being used

// @securitydefinitions.oauth2.application  OAuth2Application
// @tokenUrl                                https://example.com/oauth/token
// @scope.write                             Grants write access
// @scope.admin                             Grants read and write access to administrative information

// @securitydefinitions.oauth2.implicit  OAuth2Implicit
// @authorizationUrl                     https://example.com/oauth/authorize
// @scope.write                          Grants write access
// @scope.admin                          Grants read and write access to administrative information

// @securitydefinitions.oauth2.password  OAuth2Password
// @tokenUrl                             https://example.com/oauth/token
// @scope.read                           Grants read access
// @scope.write                          Grants write access
// @scope.admin                          Grants read and write access to administrative information

// @securitydefinitions.oauth2.accessCode  OAuth2AccessCode
// @tokenUrl                               https://example.com/oauth/token
// @authorizationUrl                       https://example.com/oauth/authorize
// @scope.admin                            Grants read and write access to administrative information

func init() {
	db = initDb()
}

func main() {
	echoInstance := initRouters(db)
	echoInstance.Logger.Fatal(echoInstance.Start(":8888"))
}

// TODO move
func initRouters(db *gorm.DB) *echo.Echo {
	e := echo.New()
	e.GET("/api/v1/events/:consignmentNum", getConsignment)
	e.GET("/swagger/*", echoSwagger.WrapHandler)
	return e
}

// TODO move
// @Summary Get Consignment Swagger Test
// @Description get consignment swagger test
// @Tags root
// @Accept json
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /events/{consignmentNum} [get]
// @Param consignmentNum path string true "ED111MY"
func getConsignment(c echo.Context) error {
	consignmentNum := c.Param("consignmentNum")
	var event EventEntity
	if db.First(&event, "consignment_no = ?", consignmentNum).Error != nil {
		return c.String(http.StatusNotFound, "not found")
	}
	dto := EventDto{
		Id:            event.ID,
		Code:          event.Code,
		ConsignmentNo: event.ConsignmentNo,
		Remarks:       event.Remarks,
	}
	return c.JSON(http.StatusOK, dto)
}

func initDb() *gorm.DB {
	db, err := gorm.Open(postgres.New(postgres.Config{
		DSN:                  "host=localhost user=gorm password=gorm dbname=postgres port=5432 sslmode=disable TimeZone=Asia/Shanghai", // data source name, refer https://github.com/jackc/pgx
		PreferSimpleProtocol: true,                                                                                                      // disables implicit prepared statement usage. By default pgx automatically uses the extended protocol
	}), &gorm.Config{})
	//sqlDb, _ := db.DB()
	//sqlDb.Exec(`set search_path='gorm'`)
	if err != nil {
		panic("failed to connect database")
	}

	// Migrate the schema
	db.AutoMigrate(&EventEntity{})

	// Create
	db.Create(&EventEntity{Code: "SIP", ConsignmentNo: "ED111MY"})

	// Read
	var event EventEntity
	db.First(&event, 1)                               // find event with integer primary key
	db.First(&event, "consignment_no = ?", "ED111MY") // find event with code D42

	// Update - update multiple fields
	db.Model(&event).Updates(EventEntity{Remarks: "Extra Remarks"}) // non-zero fields
	//db.Model(&event).Updates(map[string]interface{}{"Price": 200, "Code": "F42"})

	// Delete - delete event
	//db.Delete(&event, 1)

	return db
}
